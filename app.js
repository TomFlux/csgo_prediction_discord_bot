require('dotenv').config();

const HLTV = require('hltv-api');
const Discord = require('discord.js');
const fetch = require('node-fetch');
var fs = require('fs');

const discord = new Discord.Client();

let db = JSON.parse(fs.readFileSync('players.json', 'utf8'));

let upcomingMatches = {};

function updateDBonDisk() {
    fs.writeFile('players.json', JSON.stringify(db), 'utf8', (err) => {
        if (err) console.log(err);
        else console.log('updated DB')
    });
}

function updatePoints() {
    let alreadyDrawn = []; // API shows draws twice ree
    fetch('http://localhost:3000/results')
        .then(res => res.json())
        .then(results => {
            let stringBuild = '';
            for (let resultIDX in results) {
                let result = results[resultIDX]
                let resultID = Number(result.matchId.substring(9, 16));
                let winner;

                if (result.team1.result > result.team2.result) {
                    winner = result.team1.name;
                } else if (result.team2.result > result.team1.result) {
                    winner = result.team2.name;
                } else {
                    if (alreadyDrawn.includes(resultID)) {
                        winner = 'na'
                    } else {
                        winner = 'draw'
                        alreadyDrawn.push(resultID);
                    }
                }

                for (let userID in db) {
                    let toreAdd = []
                    db[userID].predictions.map(prediction => {
                        if (prediction.matchID === resultID) {
                            if (prediction.predictedTeam === winner) {
                                db[userID].points += 3
                                stringBuild += `${db[userID].username} got 3 points as ${prediction.predictedTeam} won.\n`;
                            } else if (winner === 'draw') {
                                db[userID].points += 1
                                stringBuild += `${db[userID].username} got 1 points as ${prediction.predictedTeam} drew.\n`;
                            } else if (winner !== 'na') {
                                stringBuild += `${db[userID].username} got 0 points as ${prediction.predictedTeam} lost.\n`;
                            }
                        } else toreAdd.push(prediction);
                    });
                    db[userID].predictions = toreAdd;
                }
            }

            if (stringBuild !== '') sendMessage(stringBuild);

            updateDBonDisk();
        });

    return fetch('http://localhost:3000/all-matches')
        .then(res => res.json())
        .then(json => upcomingMatches = json);
}

function showHelp() {
    sendMessage("__**Help**__:\n`!upcoming`\tshows upcoming games. Change page by putting the page number after the command.\n`!predict`\tpredict upcoming game. Use the format *match id* *team name*\n`!scoreboard`\tshows the current scoreboard (updated every 1000 seconds)\n!predictions\tshows your current predictions")
}

function removePrediction(m) {
    let matchID = Number(m.content.substring(8, m.content.length));
    let userID = m.author.id;
    for (let id in db) {
        if (id === userID) {
            let predictions = db[id].predictions;
            let toreAdd = []
            predictions.map(prediction => {
                if (prediction.matchID !== matchID) toreAdd.push(prediction);
            });
            db[id].predictions = toreAdd();
        }
    }
}

function searchTeam(msg) {
    const searchTeam = msg.content.substring(8, msg.content.length);
    let stringBuild = '';
    upcomingMatches.map(match => {
        match.teams.map(team => {
            if (team.name.toLowerCase() === searchTeam.toLowerCase())
                stringBuild += `${match.id}\t\`${match.teams[0].name}\`vs\`${match.teams[1].name}\``;
        });
    })

    if (stringBuild != '') sendMessage(stringBuild);
}

function showPredicted(m) {
    const id = m.author.id;
    let stringBuild = "you have currently predicted:\n";
    for (let userID in db) {
        if (userID === id) {
            db[userID].predictions.map(prediction => {
                stringBuild += `${prediction.predictedTeam} in ${prediction.matchID}`
            });
            break;
        }
    }

    sendMessage(stringBuild);
}

function showScoreboard() {
    let ps = [];
    for (let userID in db) {
        ps.push([db[userID].username, db[userID].points])
    }
    ps.sort((a, b) => b[1] - a[1]);

    let stringBuild = ""

    for (let idx in ps) {
        stringBuild += `${Number(idx) + 1}\t${ps[idx][0]}\t${ps[idx][1]}\n`
    }

    sendMessage(stringBuild);
}

function showMatches(m) {

    const page = m.content.substring(10, m.content.length) === ""
        ? 1
        : m.content.substring(10, m.content.length);

    if (page > (Math.floor(upcomingMatches.length / 10) + 1)) {
        sendMessage(`Page ${page} does no exist.`);
    } else {
        let stringBuild = '\n**ID**\t\t\t\t**Time**\t\t\t\t**Tournament**\t\t**Team 1**\t**Team 2**\t**Format**';

        upcomingMatches.slice((page - 1) * 10, page * 10).map(match => {
            stringBuild += `\n\`${match.id}\`\t`;
            if (match.event.name === '') {
                stringBuild += '\t\t\t\t\t\t ';
                // regex is get all (g) words (\w) that aren't whitespace (\s)
                // of any length (*) and pass them into the function where the
                // first letter is captialised and the rest are lowercased.
                stringBuild +=
                    match.link
                        .substring(17, match.link.length)
                        .split('-').join(' ')
                        .replace(/\w\S*/g, (txt) => {
                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        });

            } else {
                let format = 'map' in match ? 'bo1' : match.format;
                let team1 = match.teams[0].name === ""
                    ? 'TBA'
                    : match.teams[0].name;

                let team2 = match.teams[1].name === ""
                    ? 'TBA'
                    : match.teams[1].name;

                let date = new Date(match.time);
                let dateString;

                if (date.toLocaleDateString() === new Date().toLocaleDateString()) {
                    dateString = 'Today';
                } else {
                    dateString = date.toLocaleDateString();
                }

                stringBuild +=
                    `${dateString}\t${match.event.name}\t\`${team1}\`\t\`${team2}\`\t${format}`;
            }

        });
        stringBuild += `\n\`Page: ${page}/${Math.floor(upcomingMatches.length / 10) + 1}\``
        sendMessage(stringBuild);
    }
}

function predictMatch(m) {
    let predictionCommand = m.content.substring(9, m.content.length);
    if (isNaN(predictionCommand.substring(0, 7))) {
        sendMessage('please enter prediction in form "*match id* *team name*"');
    } else {
        let id = Number(predictionCommand.substring(0, 7));
        let prediction = predictionCommand.substring(8, predictionCommand.length);

        if (prediction === 'TBA') {
            sendMessage('oi stop trying to predict TBA (they\'re to good)')
        } else {
            let found = false;

            upcomingMatches.map(match => {
                if (match.id === id) {
                    match.teams.map(team => {
                        if (team.name.toLowerCase() === prediction.toLowerCase()) {
                            found = true;
                            sendMessage(updateDB(m.author, id, team.name));
                        }
                    })
                }
            });
            if (!found)
                sendMessage(`could not find team: ${prediction}`);
        }
    }
}

function updateDB(user, id, prediction) {
    // add user if user does not exist
    if (!(user.id in db)) {
        db[user.id] = {
            username: user.username,
            predictions: [],
            score: 0,
        };
    }

    let toAdd = {
        matchID: id,
        predictedTeam: prediction,
    };

    for (let i = 0; i < db[user.id]['predictions'].length; i++) {
        console.log(db[user.id]['predictions'][i]);
        if (JSON.stringify(db[user.id]['predictions'][i]) === JSON.stringify(toAdd)) {
            return `you have already predicted ${prediction} to win`;
        }
    }

    db[user.id]['predictions'].push({
        matchID: id,
        predictedTeam: prediction,
    });


    updateDBonDisk();

    return `you predicted ${prediction} to win.`;
}

function sendMessage(text) {
    const channel = discord.channels.cache.get(process.env.CHANNELID);
    channel.send(text);
}

discord.on('ready', () => {
    console.log(`Logged in as ${discord.user.tag}!`);
    upcomingMatches = updatePoints();
    setInterval(() => { upcomingMatches = updatePoints() }, 1000000);
});

discord.on('message', msg => {
    if (msg.content[0] === '!') {
        let command = msg.content.substring(1, msg.content.length);
        if (command.substring(0, 8) === 'upcoming') showMatches(msg);
        if (command.substring(0, 11) === 'predictions') showPredicted(msg);
        else if (command.substring(0, 7) === 'predict') predictMatch(msg);
        if (command.substring(0, 10) === 'scoreboard') showScoreboard(msg);
        if (command.substring(0, 6) === 'search') searchTeam(msg);
        if (command.substring(0, 4) === 'help') showHelp();
    }
});

discord.login(process.env.TOKEN);